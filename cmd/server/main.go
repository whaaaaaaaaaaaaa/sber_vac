package main

import (
	"context"
	"flag"
	"fmt"
	"golang.org/x/sync/semaphore"
	"log"
	"net"
	"os"
	"path/filepath"
	"sber_vac/protocol"
	"strings"
)

const (
	BUFF_SIZE = 1024
)

var resourceMap = make(map[string]string)

func loadResources(dir string) error {
	files, err := os.ReadDir(dir)
	if err != nil {
		return err
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}
		buf, err := os.ReadFile(filepath.Join(dir, file.Name()))
		if err != nil {
			log.Printf("Failed to load resources from %s: %v\n", file.Name(), err)
		} else {
			log.Printf("Loaded resources from %s:\n%s\n", file.Name(), string(buf))
		}

		resourceMap[file.Name()] = string(buf)
	}
	return nil
}

func handleRequest(conn *net.UDPConn, addr *net.UDPAddr, data []byte) {
	resourceName := strings.TrimSpace(string(data))
	resourceContent, exists := resourceMap[resourceName]

	var message protocol.Message
	if exists {
		message = protocol.Message{Content: resourceContent, IsError: false}
	} else {
		message = protocol.Message{Content: fmt.Sprintf("resource %s doesn't exists", resourceName), IsError: true}
	}
	log.Printf("Sending message %s to %s", message.Content, conn.RemoteAddr())

	response := message.Marshal()

	conn.WriteToUDP([]byte(response), addr)
}

func main() {

	listenAddr := flag.String("listenAddr", "127.0.0.1:8981", "listen addr")
	resourceDir := flag.String("resourceDir", "resources", "Directory containing resource files")
	semLimit := flag.Int64("semLimit", 3000, "semaphore limit")
	flag.Parse()

	err := loadResources(*resourceDir)
	if err != nil {
		fmt.Printf("Failed to load resources: %v\n", err)
		return
	}

	addr, err := net.ResolveUDPAddr("udp", *listenAddr)
	if err != nil {
		fmt.Printf("Failed to resolve address: %v\n", err)
		return
	}

	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		fmt.Printf("Failed to listen on UDP port: %v\n", err)
		return
	}

	fmt.Printf("Server is listening on %s\n", *listenAddr)

	sem := semaphore.NewWeighted(*semLimit)
	for {
		buffer := make([]byte, BUFF_SIZE)
		n, clientAddr, err := conn.ReadFromUDP(buffer)
		if err != nil {
			fmt.Printf("Error receiving data: %v\n", err)
			continue
		}

		err = sem.Acquire(context.Background(), 1)
		if err != nil {
			panic(fmt.Sprintf("Failed to acquire semaphore: %v\n", err))
		}
		go func(data []byte, addr *net.UDPAddr) {
			defer sem.Release(1)
			handleRequest(conn, addr, data[:n])
		}(buffer, clientAddr)
	}
}
