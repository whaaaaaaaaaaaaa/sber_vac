package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"sber_vac/protocol"
)

const (
	BUFF_SIZE = 1024
)

func main() {
	remoteAddr := flag.String("server_addr", "127.0.0.1:8981", "listen addr")
	resourseName := flag.String("resource", "", "Directory containing resource files")
	flag.Parse()

	if *resourseName == "" {
		fmt.Println("Usage: server -resource <resource> [-remoteAddr <address>]")
		return
	}

	serverAddr, err := net.ResolveUDPAddr("udp", *remoteAddr)
	if err != nil {
		fmt.Printf("Failed to resolve server address: %v\n", err)
		return
	}

	conn, err := net.DialUDP("udp", nil, serverAddr)
	if err != nil {
		fmt.Printf("Failed to dial server: %v\n", err)
		return
	}
	defer conn.Close()

	_, err = conn.Write([]byte(*resourseName))
	if err != nil {
		fmt.Printf("Failed to send request: %v\n", err)
		return
	}

	buffer := make([]byte, BUFF_SIZE)
	n, _, err := conn.ReadFromUDP(buffer)
	if err != nil {
		fmt.Printf("Error receiving data: %v\n", err)
		return
	}

	message, err := protocol.Unmarshal(string(buffer[:n]))
	if err != nil {
		fmt.Printf("Error unmarshaling data: %v\n", err)
		return
	}

	if message.IsError {
		fmt.Fprintf(os.Stderr, "failed to recieve resounce: %s\n", message.Content)
	} else {
		fmt.Println(message.Content)
	}
}
