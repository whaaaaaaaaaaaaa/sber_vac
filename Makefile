.PHONY: build
build:
	go get sber_vac/cmd/server
	go build -o ./dist/server ./cmd/server
	go build -o ./dist/client ./cmd/client