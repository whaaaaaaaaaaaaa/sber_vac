package protocol

import (
	"errors"
	"fmt"
	"strings"
)

const (
	BeginMarker = "-BEGIN-"
	EndMarker   = "-END-"
	ErrorMarker = "-ERROR-"
)

type Message struct {
	Content string
	IsError bool
}

func (m *Message) Marshal() string {
	if m.IsError {
		return fmt.Sprintf("%s\n%s\n%s", ErrorMarker, m.Content, EndMarker)
	}
	return fmt.Sprintf("%s\n%s\n%s", BeginMarker, m.Content, EndMarker)
}

func Unmarshal(data string) (*Message, error) {
	lines := strings.Split(data, "\n")
	if len(lines) < 3 {
		return nil, errors.New("invalid message format")
	}

	message := &Message{}
	if lines[0] == ErrorMarker {
		message.IsError = true
		message.Content = strings.Join(lines[1:len(lines)-1], "\n")
	} else if lines[0] == BeginMarker {
		message.IsError = false
		message.Content = strings.Join(lines[1:len(lines)-1], "\n")
	} else {
		return nil, errors.New("invalid message format")
	}

	if lines[len(lines)-1] != EndMarker {
		return nil, errors.New("missing end marker")
	}

	return message, nil
}
